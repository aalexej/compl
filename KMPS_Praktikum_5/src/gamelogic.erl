%%%-------------------------------------------------------------------
%%% @author andrei
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 02. Jan 2020 16:13
%%%-------------------------------------------------------------------
-module(gamelogic).
-author("andrei").
%% API
-export([updateLocalGame/2,hasWon/1,rotateBoard/1,createGameBoard/0]).

createGameBoard()->
   [[e,e,e,e,e,e,e],
    [e,e,e,e,e,e,e],
    [e,e,e,e,e,e,e],
    [e,e,e,e,e,e,e],
    [e,e,e,e,e,e,e],
    [e,e,e,e,e,e,e]].


getColumn(ColIndex,GameBoard) ->
    lists:nth(ColIndex,rotateBoard(GameBoard)).

% Matrizen-Transponieren
% https://stackoverflow.com/questions/5389254/transposing-a-2-dimensional-matrix-in-erlang
transpose([[]|_]) -> [];
transpose(M) ->
   [lists:map(fun hd/1, M) | transpose(lists:map(fun tl/1, M))].

% Tauscht Zeilen in Spalten um.
rotateBoard(GameBoard) ->
  transpose(GameBoard).

insertCoinInColumn(Spalte, Color)->
  case Spalte of
    [] -> Spalte;
    _ -> {Status,Index} = validPositionInColumnForInsert(Spalte,1),
      case Status of
        ok -> lists:sublist(Spalte, Index -1) ++ [Color] ++ lists:nthtail(Index,Spalte);
        error -> io:format("Die Spalte ist voll! ~n"), Spalte
      end
  end.

validPositionInColumnForInsert([Cell|Cells],Index) ->
  case [Cell|Cells] of
    [] -> {error,Index};
    _ -> case Cell of
           e -> {ok,Index};
           r -> validPositionInColumnForInsert(Cells,Index + 1);
           b -> validPositionInColumnForInsert(Cells,Index +1);
           _ -> {error,0}
         end
  end.

setColumn(GameBoard,SpalteNeu,SpaltenNr)->
  lists:sublist(GameBoard,SpaltenNr -1) ++ [SpalteNeu] ++ lists:nthtail(SpaltenNr,GameBoard).

% Prüft, ob Spieler Rot oder Blau gewonnen hat.
hasWon(GameBoard) ->
  case GameBoard of
    [Head|Tail] ->
      case [Head|Tail] of
        [r,r,r,r | _] -> {rWin};
        [r,_,_,_,_,_,_,r,_,_,_,_,_,_,r,_,_,_,_,_,_,r,_,_,_,_,_,_| _] -> {rWin};
        [r,_,_,_,_,_,r,_,_,_,_,_,r,_,_,_,_,_,r,_,_,_,_,_|_ ] -> {rWin};
        [r,_,_,_,_,_,_,_,r,_,_,_,_,_,_,_,r,_,_,_,_,_,_,_,r,_,_,_,_,_,_,_|_] -> {rWin};
        [b,b,b,b | _] -> {bWin};
        [b,_,_,_,_,_,_,b,_,_,_,_,_,_,b,_,_,_,_,_,_,b,_,_,_,_,_,_| _] -> {bWin};
        [b,_,_,_,_,_,b,_,_,_,_,_,b,_,_,_,_,_,b,_,_,_,_,_|_ ] -> {bWin};
        [b,_,_,_,_,_,_,_,b,_,_,_,_,_,_,_,b,_,_,_,_,_,_,_,b,_,_,_,_,_,_,_|_] -> {bWin};
        _ -> hasWon(Tail)
      end;
    [] -> {stillRunning}
  end.

% Funktion zum testen
init() ->
  Player = [r, b],
  GameBoard = createGameBoard(),
  updateLocalGame(Player,GameBoard).


updateLocalGame(Players, GameBoard) ->
  case Players of
    [CurrentPlayer,_] ->
      case CurrentPlayer of
        r -> io:format("Spieler Rot ist dran ~n"),
          Color = r;
        b -> io:format("Spieler Blau ist dran ~n"),
          Color = b
      end,
        {ok,SpaltenNr} = io:read("Geben Sie die Spaltennummer an: "),
        io:format("Spalte ~w wurde gewählt ~n",[SpaltenNr]),
        Spalte = getColumn(SpaltenNr,GameBoard),
        SpalteNeu = lists:reverse(insertCoinInColumn(lists:reverse(Spalte),Color)),
        GameBoardNeu = setColumn(rotateBoard(GameBoard),SpalteNeu,SpaltenNr),
        {lists:reverse(Players),rotateBoard(GameBoardNeu)}
  end.

