%%%-------------------------------------------------------------------
%%% @author andrei
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. Jan 2020 21:55
%%%-------------------------------------------------------------------
-module(connection).
-author("andrei").

%% API
-export([initConnection/0]).
-import(gamelogic,[updateLocalGame/2,rotateBoard/1, hasWon/1,createGameBoard/0]).
-import(gameview,[printGamePlay/1]).

initConnection() ->
  NodeName = node(),
  case NodeName of
    server@localhost -> register(gameplayer, self()), waitForMessage(client);
    client@localhost -> register(gameplayer, self()), connectToServer()
  end.


connectToServer() ->
  io:format("Versuchsaufbau zum Server ~n"),
  Status = net_kernel:connect_node(server@localhost),
  case Status of
    true ->initOnlineGame();
    false -> io:format("Es konnte keine Verbindung zum Server hergestellt werden")
  end.

initOnlineGame() ->
  Player = [r, b],
  GameBoard = gamelogic:createGameBoard(),
  printGamePlay(GameBoard),
  {Spieler, GameBoardNeu} = gamelogic:updateLocalGame(Player,GameBoard),
  sendMessageToServer(Spieler,GameBoardNeu),
  waitForMessage(server).

sendMessageToClient(Spieler,GameBoard) ->
  {gameplayer, client@localhost} ! {Spieler, GameBoard}.


sendMessageToServer(Spieler,GameBoard) ->
  {gameplayer, server@localhost} ! {Spieler,GameBoard}.


waitForMessage(From) ->
  io:format("Warte auf eingehende Nachricht vom Gegenspieler ... ~n"),
  receive
    {Spieler, GameBoard} ->
      io:format("\ec"),
      printGamePlay(GameBoard),
      GameStatus = gamelogic:hasWon(lists:append(gamelogic:rotateBoard(GameBoard))),
      case GameStatus of
        {rWin} -> io:format("Spieler Rot hat gewonnen! ~n");
        {bWin} -> io:format("Spieler Blau hat gewonnen! ~n");
        {stillRunning} ->
          {SpielerNeu, GameBoardNeu} = updateLocalGame(Spieler,GameBoard),
          printGamePlay(GameBoardNeu),
          case From of
            client -> sendMessageToClient(SpielerNeu,GameBoardNeu);
            server -> sendMessageToServer(SpielerNeu,GameBoardNeu)
          end,
          GameStatusNeu = gamelogic:hasWon(lists:append(gamelogic:rotateBoard(GameBoardNeu))),
          case GameStatusNeu of
            {rWin} -> io:format("Spieler Rot hat gewonnen! ~n");
            {bWin} -> io:format("Spieler Blau hat gewonnen! ~n");
            {stillRunning} -> waitForMessage(From)
          end
      end
  end.
