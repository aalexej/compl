%%%-------------------------------------------------------------------
%%% @author andrei
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 05. Jan 2020 21:02
%%%-------------------------------------------------------------------
-module(gameview).
-author("andrei").

%% API
-export([printGamePlay/1]).


% Funktionen, um das Spielfeld zu zeichnen
printGamePlay(GameBoard) ->
  %io:format("   Vier gewinnt! 2 Spieler  ~n"),
  io:format(" 1   2   3   4   5   6   7  ~n"),
  io:format("--------------------------- ~n"),
  printGameBoard(GameBoard).

printGameBoard([Row|Rows])->

  if
    Rows == [] ->
      printCellsFromRow(Row),
      io:format("~n");
    true ->
      printCellsFromRow(Row),
      io:format("~n"),
      printGameBoard(Rows)
  end.

printCellsFromRow([Cell|Cells])->
  if
    Cells == [] ->
      printCell(Cell),
      return;
    true ->
      printCell(Cell),
      printCellsFromRow(Cells)
  end.

printCell(Cell) ->
  if
    Cell == e ->
      io:format("[ ] ");
    Cell == r ->
      io:format("[r] ");
    Cell == b ->
      io:format("[b] ")
  end.