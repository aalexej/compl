package main

import (
	"fmt"
	"time"
)
/**
 * Programm verhält sich jetzt anders, weil die Adressen der Variablen d und dp nicht gleich sind.
 */

//iife2 Aufgabe 2
func iife2() {
	var d int = 5
	go func(dp int) {
		fmt.Println("Adresse von d: ",&d)
		fmt.Println("Adresse von dp: ",&dp)
		fmt.Println("Hello from Goroutine")
		time.Sleep(time.Duration(dp) * time.Second)
		fmt.Println("Goroutine ends after", dp, "seconds")
	}(d)

	d = 10
	time.Sleep(20 * time.Second)
	fmt.Println("Done")
}

func main() {
	iife2()
}

