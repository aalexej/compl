package main

import (
	"github.com/visualfc/atk/tk"
	"time"
)

type Window struct {
	*tk.Window
}

func NewWindow() *Window {
	mw := &Window{tk.RootWindow()}
	lbl := tk.NewLabel(mw, "Hello ATK")
	btn := tk.NewButton(mw, "Quit")
	btn.OnCommand(func() {
		tk.Quit()
	})

	computeButton := tk.NewButton(mw, "Start Computing")
	computeButton.OnCommand(func() {
		time.Sleep(10 * time.Second)
	})

	tk.NewVPackLayout(mw).AddWidgets(lbl, computeButton,tk.NewLayoutSpacer(mw, 0,true), btn)
	mw.ResizeN(300, 200)
	return mw
}

func main() {
	tk.MainLoop(func() {
		mw := NewWindow()
		mw.SetTitle("ATK Sample")
		mw.Center()
		mw.ShowNormal()
	})
}