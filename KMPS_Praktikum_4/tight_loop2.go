package main
import "fmt" ; import "time" ; import "runtime"

/**
 * 3.1 Beobachtung : Das Programm hängt
 * 3.2 Das Programm verhält sich so, weil alle Prozessorkerne mit einer Endlosschleife beschäftigt sind.
 * 4.1 Das Programm hängt nicht mehr
 * 4.2 Ein Prozessorkern ist frei und kann schließlich nach einer Sekunde bis zum Rumpfende ankommen. Wenn sich main beendet,
 * dann werde auch alle andere Go-Routinen mitbeendet.
 */

func main() {
	var x int
	procs:= runtime.GOMAXPROCS(0)-1
	fmt.Println("GOMAXPROCS: ",procs)
	for i := 0; i < procs; i++ {
		go func() {
			for { x++ }
		} ()
	}
	time.Sleep(time.Second)
	fmt.Println("x =", x)
}
