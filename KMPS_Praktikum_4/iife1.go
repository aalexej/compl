package main

import (
	"fmt"
	"time"
)

/**
 * Die Beobachtung ist, dass der Wert des Schleifenzählers in der Ausgabe bereits auf 9 ist.
 * Dieser Effekt trifft auf, weil hier eine Race-Condition stattfindet.
 */

//scoping Aufgabe 1
func iife1() {
	var TIMES = 9
	for timeAt := 0 ; timeAt < TIMES; timeAt ++ {
		go func () {
			fmt.Printf("Wert des Schleifenzählers %d\n",timeAt)
			fmt.Printf("Adresse des Schleifenzählers %x\n",&timeAt)
		}()
	}

	time.Sleep(500 * time.Millisecond)
}

func main(){
	iife1()
}


