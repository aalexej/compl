package main

import (
	"fmt"
	"time"
)

func Routine1(message chan string){
	fmt.Println(<- message)
	go func(){
		time.Sleep(500 *time.Millisecond)
		//message <- "pong"
	}()
}

func main() {

	messages := make(chan string)
	fmt.Println("Channel wurde geöffnet")
	for i:= 0; i < 2; i++{
		go func(){
			time.Sleep(500*time.Millisecond)
			messages <- "ping"
		}()
		Routine1(messages)
		// Erwartet eine Nachricht, die aber nicht ankommt.
		fmt.Println(<- messages)
	}
	fmt.Println("Channel wurde geschlossen")
	close(messages)
}