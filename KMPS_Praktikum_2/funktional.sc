import scala.io.Source

case class Track(title:String, length: String, rating:Int,
                 features:List[String],writers:List[String])

case class Album(title:String, date:String, artist:String,
                 tracks:List[Track])


def attach1(tail: List[Album], elem: Album) : List[Album] = tail match {
  case Nil => elem::Nil
  case x::xs => x::attach1(xs, elem)
}

def attach2(tail: List[Track], elem: Track) : List[Track] = tail match {
  case Nil => elem::Nil
  case x::xs => x::attach2(xs, elem)
}


def getToken(XML : List[Char], tokenList : List[String], stack: String = "") : (List[Char],List[String]) = XML match {
  case Nil => (Nil,tokenList)
  case x::xs => x match {
    case '>' => (xs,tokenList:::List(stack))
    case _ => getToken(xs, tokenList, stack + x)

  }
}

def getInnerToken(XML: List[Char], tokenList: List[String], stack : String= "") : (List[Char],List[String]) = XML match {
  case Nil => (Nil, tokenList)
  case x::xs => x match {
    case '<' => (XML,tokenList:::List(stack))
    case _ => getInnerToken(xs,tokenList,stack +x)
  }
}

def parseTrack(TokenList: List[String], track: Track = Track("","",0,List[String](),List[String]())) : (List[String],Track) = TokenList match {
  case Nil => (Nil,track)
  case x::xs => x match {
    case "/track" => (xs,track)
    case "title" => parseTrack(xs,track.copy(title = xs(0)))
    case "length" => parseTrack(xs,track.copy(length = xs(0)))
    case "rating" => parseTrack(xs,track.copy(rating = Integer.parseInt(xs(0))))
    case "feature" => parseTrack(xs, track.copy(features = track.features:::List(xs(0))))
    case "writing" => parseTrack(xs,track.copy(writers = track.writers:::List(xs(0))))
    case _ => parseTrack(xs,track)
  }
}

def parseAlbum(TokenList: List[String], album : Album = new Album("","","",tracks = List[Track]())) : (List[String],Album) = TokenList match {
  case Nil => (Nil,album)
  case x::xs => x match {
    case "/album" => (xs,album)
    case "title" => parseAlbum(xs,album.copy(title = xs(0)))
    case "date" => parseAlbum(xs,album.copy(date = xs(0)))
    case "artist" => parseAlbum(xs,album.copy(artist = xs(0)))
    case "track" => parseTrack(xs) match {
      case (Nil,trck) => (Nil,album.copy(tracks = attach2(album.tracks,trck)))
      case (ys, trck) => parseAlbum(ys, album.copy(tracks = attach2(album.tracks,trck)))
    }
    case _ => parseAlbum(xs,album)
  }
}


def parseFile(TokenList : List[String], album : List[Album] = List[Album]()) : List[Album] = TokenList match {
  case Nil => album
  case x::xs => x match {
    case "album" => parseAlbum(xs) match {
      case (Nil, alb) => attach1(album,alb)
      case (ys,alb)   => parseFile(ys, attach1(album,alb))
    }
  }
}


def createTokenList(XML : List[Char], tokenList : List[String] = List[String]()) : List[String] = XML match {
  case Nil => tokenList
  case x::xs => x match {
    case '\n' => createTokenList(xs, tokenList)
    case '\r' => createTokenList(xs,tokenList)
    case '\t' => createTokenList(xs,tokenList)
    case '>' => createTokenList(xs,tokenList)
    case '<'  => getToken(xs,tokenList) match {
      case (xml, tokenliste) => createTokenList(xml,tokenliste)
    }
    case _ => getInnerToken(XML,tokenList) match {
      case (Nil,tokenList) => tokenList
      case (xml,tokenliste) => createTokenList(xml,tokenliste)
    }
  }
}

val XMLFile: List[Char] = Source.fromFile("/home/andrei/IdeaProjects/compl/KMPS_Praktikum_2/alben.xml").mkString.toCharArray.toList;
val tokenListe = createTokenList(XMLFile)
var alben = parseFile(tokenListe)
//System.out.println(alben)


def prod(f: (Int) => Int, a: Int, b: Int): Int = b <= a match {
  case true => 1
  case false => f(b) * prod(f, a, b - 1)
}

def prodCurry(f: Int => Int): (Int, Int) => Int = {
  def prodF(a: Int, b: Int): Int = b <= a match {
    case true => 1
    case false => f(b) * prodF(a, b - 1)
  }
  prodF
}

def prodAB =
  prodCurry(x => x)


def prodFac(b: Int): Int =
  prodAB(1, b)



System.out.println(prodFac(5))
