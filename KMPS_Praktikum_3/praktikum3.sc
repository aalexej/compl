import scala.io.Source

case class Track(title:String, length: String, rating:Int,
                 features:List[String],writers:List[String])

case class Album(title:String, date:String, artist:String,
                 tracks:List[Track])


def attach1(tail: List[Album], elem: Album) : List[Album] = tail match {
  case Nil => elem::Nil
  case x::xs => x::attach1(xs, elem)
}

def attach2(tail: List[Track], elem: Track) : List[Track] = tail match {
  case Nil => elem::Nil
  case x::xs => x::attach2(xs, elem)
}


def getToken(XML : List[Char], tokenList : List[String], stack: String = "") : (List[Char],List[String]) = XML match {
  case Nil => (Nil,tokenList)
  case x::xs => x match {
    case '>' => (xs,tokenList:::List(stack))
    case _ => getToken(xs, tokenList, stack + x)

  }
}

def getInnerToken(XML: List[Char], tokenList: List[String], stack : String= "") : (List[Char],List[String]) = XML match {
  case Nil => (Nil, tokenList)
  case x::xs => x match {
    case '<' => (XML,tokenList:::List(stack))
    case _ => getInnerToken(xs,tokenList,stack +x)
  }
}

def parseTrack(TokenList: List[String], track: Track = Track("","",0,List[String](),List[String]())) : (List[String],Track) = TokenList match {
  case Nil => (Nil,track)
  case x::xs => x match {
    case "/track" => (xs,track)
    case "title" => parseTrack(xs,track.copy(title = xs(0)))
    case "length" => parseTrack(xs,track.copy(length = xs(0)))
    case "rating" => parseTrack(xs,track.copy(rating = Integer.parseInt(xs(0))))
    case "feature" => parseTrack(xs, track.copy(features = track.features:::List(xs(0))))
    case "writing" => parseTrack(xs,track.copy(writers = track.writers:::List(xs(0))))
    case _ => parseTrack(xs,track)
  }
}


def parseAlbum(TokenList: List[String], album : Album = new Album("","","",tracks = List[Track]())) : (List[String],Album) = TokenList match {
  case Nil => (Nil,album)
  case x::xs => x match {
    case "/album" => (xs,album)
    case "title" => parseAlbum(xs,album.copy(title = xs(0)))
    case "date" => parseAlbum(xs,album.copy(date = xs(0)))
    case "artist" => parseAlbum(xs,album.copy(artist = xs(0)))
    case "track" => parseTrack(xs) match {
      case (Nil,trck) => (Nil,album.copy(tracks = attach2(album.tracks,trck)))
      case (ys, trck) => parseAlbum(ys, album.copy(tracks = attach2(album.tracks,trck)))
    }
    case _ => parseAlbum(xs,album)
  }
}


def parseFile(TokenList : List[String], album : List[Album] = List[Album]()) : List[Album] = TokenList match {
  case Nil => album
  case x::xs => x match {
    case "album" => parseAlbum(xs) match {
      case (Nil, alb) => attach1(album,alb)
      case (ys,alb)   => parseFile(ys, attach1(album,alb))
    }
  }
}


def createTokenList(XML : List[Char], tokenList : List[String] = List[String]()) : List[String] = XML match {
  case Nil => tokenList
  case x::xs => x match {
    case '\n' => createTokenList(xs, tokenList)
    case '\r' => createTokenList(xs,tokenList)
    case '\t' => createTokenList(xs,tokenList)
    case '>' => createTokenList(xs,tokenList)
    case '<'  => getToken(xs,tokenList) match {
      case (xml, tokenliste) => createTokenList(xml,tokenliste)
    }
    case _ => getInnerToken(XML,tokenList) match {
      case (Nil,tokenList) => tokenList
      case (xml,tokenliste) => createTokenList(xml,tokenliste)
    }
  }
}
//KMPS PRAKTIKUM 3 BEGIN
//----------------------------------------------------------------------------------------------------------------------
def createTokenList2(xmlfile: List[Char]):List[String] = filter[String](
  poly_map[List[Char],String](
    partition[Char](
      xmlfile,
      x => x match {
        case '\t' => true
        case '\n' => true
        case '\r' => true
        case '<' => true
        case '>' => true
        case _ => false
      }),
    x => x.mkString),
  x => !x.isEmpty)

def mj_alben(alben: List[Album] ): List[Album] = alben match {
  case Nil => Nil
  case y::ys => y.artist match{
    case "Michael Jackson" => y::Nil
    case _ => mj_alben(ys)
  }
}

// Aufgabe 1
def map[A](input_list:List[A],func: A => A) : List[A]  = input_list match {
  case Nil => Nil
  case y::ys => func(y)::map(ys,func)
}


def poly_map[A,B](input_list:List[A],func: A => B) : List[B]  = input_list match {
  case Nil => Nil
  case y::ys => func(y)::poly_map(ys,func)
}


// Aufgabe 2
def filter[A](input_list:List[A],condition: A => Boolean):List[A] = input_list match {
  case Nil => Nil
  case y::ys => condition(y) match {
    case true => y::filter(ys,condition)
    case false => filter(ys,condition)
  }
}

//Aufgabe 3
def partition[A](input_list: List[A],condition: A=> Boolean):List[List[A]] = input_list match {
  case Nil => List(Nil)
  case y::ys => condition(y) match {
    case true => List(Nil):::partition(ys,condition)
    case false => partition(ys,condition) match {
      case List(ys) => List(y::ys)
      case ys::ls => (y::ys)::ls
    }
  }
}


//val listenliste = partition[Char]('a' :: 'b' :: 'c' :: 'D' :: 'e' :: 'f' :: 'G' :: 'H' :: 'i' :: 'J' :: Nil, c => c.isUpper)
//System.out.println(listenliste);
//Aufgabe 4


//FALSCH
//def sumProd(f : (Int, Int ) => Int , a:Int, b: Int ) : Int =
//  if ( a >= b)  a else f(a,sumProd(f,1+a,b))


def sumProd2(f: (Int,Int) => Int, a:Int, b:Int, zwischenErgebnis : Int ) : Int =
  if (a <= b) sumProd2(f,1+a,b,f(zwischenErgebnis,a)) else zwischenErgebnis


System.out.println(sumProd2((x,y) => x + y, 5,5,0))



def foldl(f:(Int,Int) => Int,  start : Int,  xs:List[Int]) : Int =
  xs match {
    case Nil => 0
    case x::Nil => f(start,x)
    case h::ts => foldl(f,f(start,h),ts)
  }

def range(a:Int,b:Int) : List[Int] = if (a>b) Nil else a::range(a+1,b)

def sumProd3(f:(Int,Int) => Int, a : Int, b:Int) = foldl(f,0,map[Int](range(a,b),(x) => x * x));

System.out.println(sumProd3((x,y) => x-y, 1,2));


val XMLFile: List[Char] = Source.fromFile("/home/andrei/IdeaProjects/compl/KMPS_Praktikum_3/alben.xml").mkString.toCharArray.toList;
val tokenListe = createTokenList(XMLFile)
var alben = parseFile(tokenListe)
//System.out.println(alben)
val mj_alb = mj_alben(alben);

// 1 b)-c)
val liste = map[Album](alben, x => x.copy(x.title.toUpperCase(), tracks = map[Track](x.tracks,x => x.copy(title = x.title.toUpperCase()))))
//System.out.println(liste);
//1 e)
val lenListe = poly_map[Album,List[String]](alben, x => poly_map[Track,String](x.tracks,x=>x.length ))
//System.out.println(lenListe)
// 2 b)
val ratingListe = filter[Track](mj_alb.head.tracks,x=>x.rating>=4)
//System.out.println(ratingListe);
// 2 c)
val rodTempertonListe = poly_map[Track,String](filter[Track](mj_alb.head.tracks, x => filter[String](x.writers,   x=> x match {
    case "Rod Temperton" => true
    case _ => false }) match {
    case Nil => false
    case _ => true}
),x=>x.title)
//System.out.println(rodTempertonListe);

// Aufgabe 3
val thrillerSplitListe =  partition[Track](mj_alb.head.tracks,x=>x.title match {
  case "Thriller" => true
  case _ => false
})



var tokenListe2 = createTokenList2(XMLFile);








