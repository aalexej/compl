def prod(f: (Int) => Int, a: Int, b: Int): Int = b <= a match {
  case true => 1
  case false => f(b) * prod(f, a, b - 1)
}

def prodCurry(f: Int => Int): (Int, Int) => Int = {
  def prodF(a: Int, b: Int): Int = b <= a match {
    case true => 1
    case false => f(b) * prodF(a, b - 1)
  }
  prodF
}

def prodAB =
  prodCurry(x => x)


def prodFac(b: Int): Int =
  prodAB(1, b)