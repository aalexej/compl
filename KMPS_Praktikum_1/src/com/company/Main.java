package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        //main.showRecursiveOutput();
        main.showImperativOutput();
    }

    public void showRecursiveOutput() throws IOException{
        String file_path = ("xmldata/alben.xml");
        byte[] file_contents = Files.readAllBytes(Paths.get(file_path));
        String dataContent = new String(file_contents, StandardCharsets.UTF_8);
        //System.out.println(dataContent);

        ArrayList<String> tokenListe = new ArrayList<String>();
        Rekurisv rekurisv = new Rekurisv();
        tokenListe = rekurisv.createTokenList(dataContent,new ArrayList<String>(),"");
        //System.out.println(tokenListe);
        ArrayList<Album> alben = rekurisv.parseFile(tokenListe,new ArrayList<Album>());


        for(int i = 0; i < alben.size(); i++){
            System.out.println(alben.get(i));
        }
    }

    public void showImperativOutput(){
        Imperativ imperativParser = new Imperativ();

        try{
            imperativParser.parseAlbumImperativ();
        }catch (IOException ex) {
            System.err.println(ex.toString());
        }
    }
}
