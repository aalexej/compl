package com.company;

import java.util.ArrayList;

public class Rekurisv {

    final String OPENED_ALBUM = "<album>";
    final String CLOSED_ALBUM = "</album>";
    final String OPENED_TITLE = "<title>";
    final String CLOSED_TITLE = "</title>";
    final String OPENED_ARTIST = "<artist>";
    final String CLOSED_ARTIST = "</artist>";
    final String OPENED_DATE = "<date>";
    final String CLOSED_DATE = "</date>";

    final String OPENED_TRACK = "<track>";
    final String CLOSED_TRACK = "</track>";
    final String OPENED_FEATURE = "<feature>";
    final String CLOSED_FEATURE = "</feature>";
    final String OPENED_TRACK_TITLE = "<title>";
    final String CLOSED_TRACK_TITLE = "</title>";
    final String OPENED_TRACK_LENGTH = "<length>";
    final String CLOSED_TRACK_LENGTH = "</length>";
    final String OPENED_TRACK_WRITING = "<writing>";
    final String CLOSED_TRACK_WRITING = "</writing>";
    final String OPENED_TRACK_RATING = "<rating>";
    final String CLOSED_TRACK_RATING = "</rating>";

    /**
     *
     * @param dataContent XML-Album als String
     * @param tokenList Leere Liste für Tokens
     * @param queue Leerer Stack
     * @return
     */
    public ArrayList<String> createTokenList(String dataContent, ArrayList<String> tokenList, String queue) {

        int currentCharacter = 0;

        if(dataContent.charAt(currentCharacter) == '\n' || dataContent.charAt(currentCharacter) == '\r' || dataContent.charAt(currentCharacter) == '\t'){
            currentCharacter++;
        }else if(dataContent.charAt(currentCharacter) == '<' && !queue.isEmpty()){
            tokenList.add(queue);
            queue = "";
        }
        else if(dataContent.substring(currentCharacter,currentCharacter + OPENED_ALBUM.length()).equals(OPENED_ALBUM)){

            currentCharacter += OPENED_ALBUM.length();
            tokenList.add("album");
        }
        else if(dataContent.substring(currentCharacter,currentCharacter + CLOSED_ALBUM.length()).equals(CLOSED_ALBUM)){
            currentCharacter += CLOSED_ALBUM.length();
            tokenList.add("/album");
        }
        else if(dataContent.substring(currentCharacter,currentCharacter + OPENED_TITLE.length()).equals(OPENED_TITLE)){
            currentCharacter += OPENED_TITLE.length();
            tokenList.add("title");
        }else if(dataContent.substring(currentCharacter,currentCharacter+ CLOSED_TITLE.length()).equals(CLOSED_TITLE)){
            currentCharacter += CLOSED_TITLE.length();
            tokenList.add("/title");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_ARTIST.length()).equals(OPENED_ARTIST)){
            currentCharacter += OPENED_ARTIST.length();
            tokenList.add("artist");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_ARTIST.length()).equals(CLOSED_ARTIST)){
            currentCharacter +=  CLOSED_ARTIST.length();
            tokenList.add("/artist");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_DATE.length()).equals(OPENED_DATE)){
            currentCharacter += OPENED_DATE.length();
            tokenList.add("date");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_DATE.length()).equals(CLOSED_DATE)){
            currentCharacter += CLOSED_DATE.length();
            tokenList.add("/date");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_TRACK.length()).equals(OPENED_TRACK)){
            currentCharacter += OPENED_TRACK.length();
            tokenList.add("track");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_TRACK.length()).equals(CLOSED_TRACK)){
            currentCharacter += CLOSED_TRACK.length();
            tokenList.add("/track");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_FEATURE.length()).equals(OPENED_FEATURE)){
            currentCharacter += OPENED_FEATURE.length();
            tokenList.add("feature");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_FEATURE.length()).equals(CLOSED_FEATURE)){
            currentCharacter += CLOSED_FEATURE.length();
            tokenList.add("/feature");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_TRACK_TITLE.length()).equals(OPENED_TRACK_TITLE)){
            currentCharacter += OPENED_TRACK_TITLE.length();
            tokenList.add("title");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_TRACK_TITLE.length()).equals(CLOSED_TRACK_TITLE)){
            currentCharacter += CLOSED_TRACK_TITLE.length();
            tokenList.add("/title");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_TRACK_LENGTH.length()).equals(OPENED_TRACK_LENGTH)) {
            currentCharacter += OPENED_TRACK_LENGTH.length();
            tokenList.add("length");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_TRACK_LENGTH.length()).equals(CLOSED_TRACK_LENGTH)) {
            currentCharacter += CLOSED_TRACK_LENGTH.length();
            tokenList.add("/length");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_TRACK_WRITING.length()).equals(OPENED_TRACK_WRITING)) {
            currentCharacter += OPENED_TRACK_WRITING.length();
            tokenList.add("writing");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_TRACK_WRITING.length()).equals(CLOSED_TRACK_WRITING)) {
            currentCharacter += CLOSED_TRACK_WRITING.length();
            tokenList.add("/writing");
        }else if(dataContent.substring(currentCharacter,currentCharacter+OPENED_TRACK_RATING.length()).equals(OPENED_TRACK_RATING)) {
            currentCharacter += OPENED_TRACK_RATING.length();
            tokenList.add("rating");
        }else if(dataContent.substring(currentCharacter,currentCharacter+CLOSED_TRACK_RATING.length()).equals(CLOSED_TRACK_RATING)) {
            currentCharacter += CLOSED_TRACK_WRITING.length();
            tokenList.add("/rating");
        }else{
            queue += dataContent.charAt(0);
            currentCharacter++;
        }

        //Rekursiver Aufruf: Falls Liste leer ist, gib Tokenliste zurück

        if (currentCharacter < dataContent.length()) {
            createTokenList(dataContent.substring(currentCharacter), tokenList, queue);
        }
        return tokenList;
    }

    public ArrayList<Album> parseFile(ArrayList<String> tokenList, ArrayList<Album> alben){
        if (tokenList.isEmpty()){
            return alben;
        }else if(tokenList.get(0).equals("album")){
            tokenList.remove(0);
            alben.add(parseAlbum(tokenList,new Album()));
            parseFile(tokenList,alben);
        }else{
            tokenList.remove(0);
            parseFile(tokenList,alben);
        }

        return alben;
    }

    public Album parseAlbum(ArrayList<String> tokenList,Album album) {
        if (tokenList.get(0).equals("/album")){
            tokenList.remove(0);
            return album;
        }else if(tokenList.get(0).equals("track")){
            tokenList.remove(0);
            album.tracks.add(parseTrack(tokenList,new Track()));
            parseAlbum(tokenList,album);
        }else if(tokenList.get(0).equals("title")){
            album.title = tokenList.get(1);
            parseAlbum(removeItemsFromFirst(tokenList,3),album);
        }else if(tokenList.get(0).equals("date")){
            album.date = tokenList.get(1);
            parseAlbum(removeItemsFromFirst(tokenList,3),album);
        }else if(tokenList.get(0).equals("artist")){
            album.artist = tokenList.get(1);
            parseAlbum(removeItemsFromFirst(tokenList,3),album);
        }

        return album;
    }

    public Track parseTrack(ArrayList<String> tokenList, Track track){
        if(tokenList.get(0).equals("/track")){
            tokenList.remove(0);
            return track;
        }else if (tokenList.get(0).equals("title")){
            track.title = tokenList.get(1);
            parseTrack(removeItemsFromFirst(tokenList,3),track);
        }else if(tokenList.get(0).equals("length")){
            track.length = tokenList.get(1);
            parseTrack(removeItemsFromFirst(tokenList,3),track);
        }else if(tokenList.get(0).equals("rating")){
            track.rating = Integer.parseInt(tokenList.get(1));
            parseTrack(removeItemsFromFirst(tokenList,3),track);
        }else if(tokenList.get(0).equals("feature")){
            track.features.add(tokenList.get(1));
            parseTrack(removeItemsFromFirst(tokenList,3),track);
        }else if(tokenList.get(0).equals("writing")){
            track.writers.add(tokenList.get(1));
            parseTrack(removeItemsFromFirst(tokenList,3),track);
        }
        return track;
    }

    public ArrayList<String> removeItemsFromFirst(ArrayList<String> list ,int count){
        if(count > 0) {
            list.remove(0);
            removeItemsFromFirst(list, count - 1);
        }
        return list;
    }

}
